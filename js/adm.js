$(document).ready(function(){
		$('body').layout({north__size:90});
		
		$('#topo').css('background','url("/img/topo_backlinha.jpg") repeat-x');
		
	
		
		$('#accordion div h3').css('padding','5px 5px 5px 25px');
		$(".ui-accordion-content").css("padding","5px 10px 5px 25px");
		
		$(".data").datepicker({
			dateFormat: 'dd/mm/yy',
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			nextText: 'Próximo',
			prevText: 'Anterior'
		});
		
		$('form').validationEngine();
		
		$('.excluir').click(function(){
			if(!confirm('Deseja realmente excluir o registro?')){
				return false;
			}
		});
		
	});