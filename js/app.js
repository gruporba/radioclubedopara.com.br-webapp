var app = angular.module('app', ['ngSanitize']);
app.config(function($httpProvider){
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

app.controller('tabelaJogos', function($scope, $http, $sce, $interval) {
	$scope.loadTabelaJogos = function () {
		$http.get("http://radioclubedopara.com.br/api/jogos/index.php")
		.then(
			function(response) {
			if ( !response.data == undefined || !response.data == '' ||  !response.data == null || !response == "Sem placar no momento." ) {
				$scope.semPlacar = false;
				$scope.jogos = response.data.Jogos;
			   angular.forEach(response.data["Jogos"], function(value, key) {
				  //console.log($sce.trustAsHtml(response.data["Jogos"][key].info));
				  response.data["Jogos"][key].info = $sce.trustAsHtml(response.data["Jogos"][key].info);
				});

			} else {
				$scope.semPlacar = true;
			}
		},
			function(error) {
			alert('Não foi possível carregar os jogos');
			console.log(error);
		});
	};
	$scope.loadTabelaJogos();

	// Reload tabelaJogos a cada 10 minutos
	$interval(function(){
		$scope.loadTabelaJogos();
	},120000);
});

app.controller('ouvintesOnline', function($scope, $http, $interval) {
	$scope.loadOuvintesOnline = function() {
		$http.get("http://radioclubedopara.com.br/api/ouvintes/")
		.then(
			function(response) {
				if ( !response.data == undefined || !response.data == '' ||  !response.data == null) {
					$scope.ouvintes = response.data.Ouvintes[0].nome;
					$scope.comOuvintes = true;
				} else {
					$scope.comOuvintes = false;
				}
			},
			function(error) {
			alert('Não foi possível carregar os jogos');
			console.log(error);
		});
	};

	$scope.loadOuvintesOnline();

	// Reload ouvintes a cada 10 minutos
	$interval(function(){
		$scope.loadOuvintesOnline();
	},120000);

});
